import * as dotenv from 'dotenv';
dotenv.config();
import koa from 'koa';
import mongoose from 'mongoose';
import bodyParser from 'koa-bodyparser';
import session from 'koa-session';
import MongooseStore from 'koa-session-mongoose';
import userRouter from '#routes/user.js'
import authService from '#services/auth.js'
import wasteRouter from "#routes/waste.js";
import wasteCategoryRouter from "#routes/waste-category.js";

const app = new koa();
app.keys = [process.env.APP_KEY_1, process.env.APP_KEY_2, process.env.APP_KEY_3];

try {
    const mongooseConnection = await mongoose.connect(process.env.MONGO_URL, {
            user: process.env.MONGO_USER,
            pass: process.env.MONGO_PASS,
            dbname: process.env.MONGO_DB_NAME,
            useNewUrlParser: true,
            useUnifiedTopology: true,
    });
    console.log('MongoDB database Connected...');

    app.use(session({
        key: 'session',
        sameSite: 'Strict',
        store: new MongooseStore({
            connection: mongooseConnection,
            expires: 86400,
        })
    }, app))
} catch (e) {
    console.error(e);
}

app.use(bodyParser());

app.use(userRouter.routes());
app.use(authService);
app.use(wasteRouter.routes());
app.use(wasteCategoryRouter.routes());

app.listen(process.env.PORT || 3001, '0.0.0.0', () => {
    console.log(`App listening at http://localhost:${process.env.PORT || 3001}`)
});

