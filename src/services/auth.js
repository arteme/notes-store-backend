import responseMessages from "#constants/response-messages.js";

export default (ctx, next) =>  {
    if (ctx.session.userId) {
        ctx.userId = ctx.session.userId;
        return next();
    }

    ctx.status = 401;
    ctx.body = {
        message: responseMessages.NOT_AUTHENTICATED,
    }
}