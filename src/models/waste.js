import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const WasteScheme = new Schema({
    userId: String,
    name: {
        type: String,
        required: true,
    },
    categories: {
        type: Array,
        required: true,
    },
    createdAt: String,
    description: String,
});

export default model('waste', WasteScheme);
