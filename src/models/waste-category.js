import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const WasteCategoryScheme = new Schema({
    userId: String,
    name: {
        type: String,
        required: true,
    },
});

export default model('wasteCategory', WasteCategoryScheme);
