export default {
    NOT_AUTHENTICATED: 'You are not authenticated!',
    NOT_FOUND: 'Not found',
    FAILED: 'Failed',
}