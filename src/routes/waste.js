import Router from "@koa/router";
import Waste from "#models/waste.js";
import responseMessages from "#constants/response-messages.js";

const router = new Router({
    prefix: '/wastes'
});

router.get('/', async (ctx) => {
    try {
        const wastes = await Waste.find({ userId: ctx.userId }).exec();
        if (!wastes) {
            ctx.status = 404;
            return ctx.body = {
                message: responseMessages.NOT_FOUND
            }
        }

        ctx.status = 200;
        ctx.body = wastes;
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
});

router.post('/', async (ctx) => {
    const newWaste = new Waste();
    newWaste.userId = ctx.userId;
    newWaste.name = ctx.request.body.name;
    newWaste.categories = ctx.request.body.categories;
    newWaste.createdAt = new Date().toISOString();
    newWaste.description = ctx.request.body.description;

    try {
        const waste = await newWaste.save();
        if (!waste) {
            ctx.status = 500;
            ctx.body = {
                message: responseMessages.FAILED
            }
            return;
        }

        ctx.status = 200;
        ctx.body = waste;
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
});

router.get('/:id', async (ctx) => {
    try {
        const waste = await Waste.findById(ctx.params.id);
        if (!waste || waste.userId !== ctx.userId) {
            ctx.status = 404;
            return ctx.body = {
                message: responseMessages.NOT_FOUND
            }
        }

        ctx.status = 200;
        ctx.body = waste;
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
});

router.put('/:id', async (ctx) => {
    try {
        const waste = await Waste.findById(ctx.params.id);
        if (!waste || waste.userId !== ctx.userId) {
            ctx.status = 404;
            return ctx.body = {
                message: responseMessages.NOT_FOUND
            }
        }

        const res = await waste.updateOne(ctx.request.body);
        if (!res?.acknowledged) {
            ctx.status = 500;
            return ctx.body = {
                message: responseMessages.FAILED
            }
        }

        ctx.status = 200;
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
});

router.del('/:id', async (ctx) => {
    try {
        const waste = await Waste.findById(ctx.params.id);
        if (!waste || waste.userId !== ctx.userId) {
            ctx.status = 404;
            return ctx.body = {
                message: responseMessages.NOT_FOUND
            }
        }

        const res = await waste.deleteOne();
        if (!res) {
            ctx.status = 500;
            return ctx.body = {
                message: responseMessages.FAILED
            }
        }

        ctx.status = 200;
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
});

export default router;