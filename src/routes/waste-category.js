import Router from "@koa/router";
import WasteCategory from "#models/waste-category.js";
import responseMessages from "#constants/response-messages.js";

const router = new Router({
    prefix: '/waste-categories'
});

router.get('/', async (ctx) => {
    try {
        const wastes = await WasteCategory.find({ userId: ctx.userId }).exec();
        if (!wastes) {
            ctx.status = 404;
            return ctx.body = {
                message: responseMessages.NOT_FOUND
            }
        }

        ctx.status = 200;
        ctx.body = wastes;
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
});

router.post('/', async (ctx) => {
    const newWasteCategory = new WasteCategory();
    newWasteCategory.userId = ctx.userId;
    newWasteCategory.name = ctx.request.body.name;

    try {
        const waste = await newWasteCategory.save();
        if (!waste) {
            ctx.status = 500;
            ctx.body = {
                message: responseMessages.FAILED
            }
            return;
        }

        ctx.status = 200;
        ctx.body = waste;
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
});

router.get('/:id', async (ctx) => {
    try {
        const wasteCategory = await WasteCategory.findById(ctx.params.id);
        if (!wasteCategory || wasteCategory.userId !== ctx.userId) {
            ctx.status = 404;
            return ctx.body = {
                message: responseMessages.NOT_FOUND
            }
        }

        ctx.status = 200;
        ctx.body = wasteCategory;
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
});

router.put('/:id', async (ctx) => {
    try {
        const wasteCategory = await WasteCategory.findById(ctx.params.id);
        if (!wasteCategory || wasteCategory.userId !== ctx.userId) {
            ctx.status = 404;
            return ctx.body = {
                message: responseMessages.NOT_FOUND
            }
        }

        const res = await wasteCategory.updateOne(ctx.request.body);
        if (!res?.acknowledged) {
            ctx.status = 500;
            return ctx.body = {
                message: responseMessages.FAILED
            }
        }

        ctx.status = 200;
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
});

router.del('/:id', async (ctx) => {
    try {
        const wasteCategory = await WasteCategory.findById(ctx.params.id);
        if (!wasteCategory || wasteCategory.userId !== ctx.userId) {
            ctx.status = 404;
            return ctx.body = {
                message: responseMessages.NOT_FOUND
            }
        }

        const res = await wasteCategory.deleteOne();
        if (!res) {
            ctx.status = 500;
            return ctx.body = {
                message: responseMessages.FAILED
            }
        }

        ctx.status = 200;
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
});

export default router;