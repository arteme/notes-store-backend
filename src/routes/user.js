import Router from '@koa/router';
import User from '#models/user.js'
import responseMessages from "#constants/response-messages.js";
import authService from '#services/auth.js';

const router = new Router({
    prefix: '/user'
});

router.post('/login', async (ctx) => {
    try {
        const user = await User.findOne({ email : ctx.request.body.email });
        if (!user) {
            ctx.status = 401;
            ctx.body = {
                message: responseMessages.NOT_AUTHENTICATED
            }
            return;
        }

        if (user.validatePassword(ctx.request.body.password)) {
            ctx.session.userId = user._id;
            ctx.status = 200
            return;
        }

        ctx.status = 401;
        ctx.body = {
            message: responseMessages.NOT_AUTHENTICATED
        }
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
});

router.post('/register', async (ctx) => {
    const newUser = new User();
    newUser.email = ctx.request.body.email;
    newUser.setPassword(ctx.request.body.password);

    try {
        const user = await newUser.save();
        if (!user) {
            ctx.status = 500;
            ctx.body = {
                message: responseMessages.FAILED
            }
            return;
        }

        ctx.session.userId = user._id;
        ctx.status = 200;
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
})

router.get('/me', authService, async (ctx) => {
    try {
        const user = await User.findById(ctx.userId);
        if (!user) {
            ctx.status = 401;
            ctx.body = {
                message: responseMessages.NOT_AUTHENTICATED
            }
            return;
        }

        ctx.status = 200;
        ctx.body = {
            id: user._id,
            email: user.email
        };
    } catch (e) {
        ctx.status = 500;
        ctx.body = {
            message: e.message
        }
    }
})

export default router;